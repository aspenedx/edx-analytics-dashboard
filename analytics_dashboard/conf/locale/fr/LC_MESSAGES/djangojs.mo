��          �      |      �     �                         )  
   0     ;     @     M     `     f     n          �  f   �               $     -     =  �  I     �     �                    !     '     6     <     J     ^     d     p     �     �  �   �     Z     i  	   �     �     �                     
                                	                                            Active Students Age Country Date Educational Background Female Honor Code Male Not Reported Number of Students Other Percent Percent of Total Posted in Forum Professional Student location is determined from IP address. This map shows where students most recently connected. Total Enrollment Tried a Problem Verified Watched a Video Week Ending Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-24 16:24-0500
PO-Revision-Date: 2014-11-24 21:27+0000
Last-Translator: cblackburn <cblackburn@edx.org>
Language-Team: French (http://www.transifex.com/projects/p/edx-platform/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Étudiants actifs Age Pays Date Parcours de Formation Femme Code d'honneur Homme Non Rapporté Nombre d'étudiants Autre Pourcentage Pourcentage du Total Ont posté dans un forum Professionnel La localisation de l'étudiant est déterminée à partir de l'adresse IP. Cette carte montre les lieux depuis lesquels les étudiants se sont le plus récemment connectés. Effectif total Ont travaillé sur un problème Vérifié Ont regardé une vidéo Semaine se terminant 