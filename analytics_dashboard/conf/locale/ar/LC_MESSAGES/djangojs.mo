��          �      |      �     �                         )  
   0     ;     @     M     `     f     n          �  f   �               $     -     =  �  I  !   ?  
   a     l     y  !   �     �     �     �     �     �            &   -  J   T     �  �   �  ,   f  F   �     �  9   �  #   1                     
                                	                                            Active Students Age Country Date Educational Background Female Honor Code Male Not Reported Number of Students Other Percent Percent of Total Posted in Forum Professional Student location is determined from IP address. This map shows where students most recently connected. Total Enrollment Tried a Problem Verified Watched a Video Week Ending Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-24 16:24-0500
PO-Revision-Date: 2014-11-24 21:27+0000
Last-Translator: cblackburn <cblackburn@edx.org>
Language-Team: Arabic (http://www.transifex.com/projects/p/edx-platform/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 الطلّاب المشاركون العمر الدولة التاريخ  الخلفية التعليمية أنثى ميثاق الشرف  ذكر لم يُبلَّغ عنه عدد الطلّاب غير ذلك النسبة المئوية النسبة مقابل المجموع الطلاب الذين شاركوا بمنشورات في المنتدى  مهني يُحدَّد مكان الطالب عبر عنوان الـ IP الخاص به. وتبيّن هذه الخارطة المواقع التي اتّصل منها الطلّاب مؤخرًا. إجمالي الطلاب المسجّلين الطلاب الذين حاولوا أن يحلّوا مسألة ما جرى التحقّق منه الطلاب الذين شاهدوا شريط فيديو   الأسبوع المنتهي في 